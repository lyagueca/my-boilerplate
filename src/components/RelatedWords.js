import PropTypes from 'prop-types';
import React from 'react';
import Word from './Word';

const RelatedWords = props => (
  <div className="App-related-words">
    <h4>Words related to &quot;{props.selected}&quot;</h4>
    {props.words.length ? (
      props.words.map(w => <Word key={w} word={w} />)
    ) : (
      <p>No related words were found.</p>
    )}
  </div>
);

RelatedWords.defaultProps = {
  words: [],
};

RelatedWords.propTypes = {
  selected: PropTypes.string.isRequired,
  words: PropTypes.array,
};

export default RelatedWords;
