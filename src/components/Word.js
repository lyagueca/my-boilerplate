import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

const Word = props => (
  <div className="App-word">
    <Link to={['/word/', props.word].join('')}>{props.word}</Link>
  </div>
);

Word.propTypes = {
  word: PropTypes.string.isRequired,
};

export default Word;
